const webpack = require("webpack");
const path = require("path");
const extract = require("mini-css-extract-plugin");

module.exports = {
    entry: "./src/index.ts",
    output: {
        filename: "bundle.js",
        path: __dirname + "/static"
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: "ts-loader"
            },
            {
                test: /\.css$/,
                use: [extract.loader, "css-loader"]
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js"],
        mainFields: ["browser", "main"],
        alias: {
            "@components": path.resolve(__dirname, "./src/components")
        }
    },
    plugins: [
        new extract({
            filename: "bundle.css"
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, "static"),
        port: 9000,
        host: "0.0.0.0"
    }
};
