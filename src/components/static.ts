export function Static(name: string, description: string): HTMLDivElement {
    const row = document.createElement("div");
    const column = document.createElement("div");

    row.className = "row";
    column.className = "col-md-12";

    if (name) {
        const title = document.createElement("h3");

        title.textContent = name;

        column.appendChild(title);
    }

    if (description) {
        const p = document.createElement("p");

        p.textContent = description;

        column.appendChild(p);
    }

    row.appendChild(column);

    return row;
}
