export function Progressbar(id: string): HTMLDivElement {
    const row = document.createElement("div");
    const column = document.createElement("div");
    const outer = document.createElement("div");
    const inner = document.createElement("div");

    row.className = "row";
    column.className = "col-xs-12";
    outer.className = "progress";
    inner.className = "progress-bar progress-bar-warning progress-bar-striped";

    inner.id = `progress-bar-${id}`;
    inner.setAttribute("role", "progressbar");
    inner.setAttribute("aria-valuenow", "0");
    inner.setAttribute("aria-valuemin", "0");
    inner.setAttribute("aria-valuemax", "100");
    inner.style.width = "0%";

    outer.appendChild(inner);
    column.appendChild(outer);
    row.appendChild(column);

    return row;
}
