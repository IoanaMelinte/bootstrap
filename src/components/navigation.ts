import { Progressbar } from "./progressbar";

export function Navigation(id: string, fnNext: () => void, fnPrevious: () => void): HTMLElement {
    const navigation = document.createElement("nav");
    const container = document.createElement("div");
    const row = document.createElement("div");
    const left = document.createElement("div");
    const right = document.createElement("div");
    const buttonPrevious = document.createElement("button");
    const buttonNext = document.createElement("button");
    const label = document.createElement("span");

    navigation.className = "navbar navbar-default navbar-fixed-bottom footer";
    container.className = "container";
    row.className = "row";
    left.className = "pull-left buttons-left";
    right.className = "pull-right buttons-right";

    buttonPrevious.id = `button-previous-${id}`;
    buttonPrevious.className = "btn btn-default btn-lg";
    buttonPrevious.setAttribute("type", "button");
    buttonPrevious.setAttribute("disabled", "disabled");
    buttonPrevious.appendChild(document.createElement("i")).className = "fa fa-fw fa-arrow-left";
    buttonPrevious.addEventListener("click", () => fnPrevious());

    const buttonPreviousLabel = buttonPrevious.appendChild(document.createElement("span"));

    buttonPreviousLabel.className = "hidden-sm hidden-xs";
    buttonPreviousLabel.textContent = " Back";

    const buttonNextLabel = buttonNext.appendChild(document.createElement("span"));

    buttonNextLabel.className = "hidden-sm hidden-xs";
    buttonNextLabel.textContent = "Next ";

    buttonNext.id = `button-next-${id}`;
    buttonNext.className = "btn btn-default btn-lg";
    buttonNext.setAttribute("type", "button");
    buttonNext.setAttribute("disabled", "disabled");
    buttonNext.appendChild(document.createElement("i")).className = "fa fa-fw fa-arrow-right";
    buttonNext.addEventListener("click", () => fnNext());

    label.className = "progress-text hidden-xs";
    label.appendChild(document.createElement("span")).textContent = "You are at ";

    const labelText = label.appendChild(document.createElement("span"));

    labelText.id = `progress-label-${id}`;
    labelText.className = "text-primary";
    labelText.textContent = "0%";

    label.appendChild(document.createElement("span")).textContent = " of the form.";

    left.appendChild(buttonPrevious);
    left.appendChild(label);
    right.appendChild(buttonNext);
    row.appendChild(left);
    row.appendChild(right);
    container.appendChild(row);
    container.appendChild(Progressbar(id));
    navigation.appendChild(container);

    return navigation;
}
