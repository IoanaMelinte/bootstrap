export function Content(id: string): HTMLElement {
    const content = document.createElement("section");

    content.id = `content-${id}`;

    return content;
}
