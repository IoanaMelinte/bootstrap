import * as Tripetto from "tripetto-collector";

export function Group(id: string, name: string, required: boolean | undefined, description: string, explanation: string): HTMLDivElement {
    const group = document.createElement("div");

    group.className = "form-group";

    if (name !== "") {
        const label = document.createElement("label");

        label.className = "col-sm-12 control-label";
        label.textContent = name + " ";
        label.setAttribute("for", id);

        if (required) {
            const asterisk = document.createElement("span");

            asterisk.className = "form-required";
            asterisk.textContent = "* ";

            label.appendChild(asterisk);
        }

        if (explanation !== "") {
            const popover = document.createElement("span");

            popover.className = "fa fa-info-circle form-hint";
            popover.setAttribute("role", "button");
            popover.setAttribute("data-toggle", "popover");
            popover.setAttribute("data-placement", "top");
            popover.setAttribute("title", "More information");
            popover.setAttribute("data-content", explanation);

            label.appendChild(popover);

            Tripetto.F.ScheduleAction(() => {
                new Function("", `$(".form-hint").popover();`)();
            });
        }

        group.appendChild(label);
    }

    if (description !== "") {
        const div = document.createElement("div");

        div.className = "col-sm-12 help-block";
        div.textContent = description;

        group.appendChild(div);
    }

    return group;
}
