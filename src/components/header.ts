export function Header(title: string): HTMLElement {
    const header = document.createElement("section");
    const container = document.createElement("div");
    const row = document.createElement("div");
    const column = document.createElement("div");
    const h1 = document.createElement("h1");

    header.className = "header";
    container.className = "container";
    row.className = "row";
    column.className = "col-md-12";
    h1.textContent = title;

    column.appendChild(h1);
    row.appendChild(column);
    container.appendChild(row);
    header.appendChild(container);

    return header;
}
