import * as Tripetto from "tripetto-collector";
import { IRadiobutton, IRadiobuttons } from "tripetto-block-radiobuttons";
import { Group } from "@components/group";

@Tripetto.node("tripetto-block-radiobuttons")
export class Radiobuttons extends Tripetto.NodeBlock<HTMLElement, IRadiobuttons> {
    private Update(data: Tripetto.Data<string>, id: string | undefined): void {
        const value = Tripetto.F.FindFirst(this.Props.Radiobuttons, (radiobutton: IRadiobutton) => radiobutton.Id === id);

        data.Set(value ? value.Value || value.Name : value, id);
    }

    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const button = this.DataAssert<string>(instance, "button");
        const group = document.createElement("div");
        const field = document.createElement("div");
        const table = document.createElement("table");
        const tbody = document.createElement("tbody");
        const selected =
            Tripetto.F.FindFirst(this.Props.Radiobuttons, (radiobutton: IRadiobutton) =>
                Tripetto.F.CastToBoolean(button.Reference === radiobutton.Id)
            ) || Tripetto.F.ArrayItem(this.Props.Radiobuttons, 0);

        if (selected) {
            this.Update(button, selected.Id);
        }

        field.className = "col-sm-12";
        table.className = "table table-striped table-options";

        Tripetto.F.Each(this.Props.Radiobuttons, (radiobutton: IRadiobutton) => {
            const tr = document.createElement("tr");
            const td = document.createElement("td");
            const div = document.createElement("div");
            const label = document.createElement("label");
            const input = document.createElement("input");
            const span = document.createElement("span");

            div.className = "radio";
            span.textContent = radiobutton.Name;
            input.setAttribute("type", "radio");
            input.name = `radiobuttons-${this.Node.Props.Id}`;
            input.id = `radiobutton-${radiobutton.Id}`;

            if (selected === radiobutton) {
                input.checked = true;
            }

            input.addEventListener("propertychange", () => this.Update(button, radiobutton.Id));
            input.addEventListener("change", () => this.Update(button, radiobutton.Id));
            input.addEventListener("blur", () => this.Update(button, radiobutton.Id));

            label.appendChild(input);
            label.appendChild(span);
            div.appendChild(label);
            td.appendChild(div);
            tr.appendChild(td);
            tbody.appendChild(tr);
        });

        table.appendChild(tbody);
        field.appendChild(table);

        group
            .appendChild(
                Group(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    false,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(field);

        return group;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
