import * as Tripetto from "tripetto-collector";
import { IEmail } from "tripetto-block-email";
import { Group } from "@components/group";

/* tslint:disable-next-line:max-line-length */
const IS_EMAIL = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

@Tripetto.node("tripetto-block-email")
export class Email extends Tripetto.NodeBlock<HTMLElement, IEmail> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const slot = this.SlotAssert("email");
        const email = this.DataAssert<string>(instance, slot);
        const group = document.createElement("div");
        const field = document.createElement("div");
        const input = document.createElement("input");

        field.className = "col-sm-12";
        input.className = "form-control input-lg";

        input.setAttribute("type", "email");
        input.setAttribute("id", this.Node.Props.Id);

        if (Tripetto.F.IsFilledString(this.Node.Props.Placeholder)) {
            input.setAttribute("placeholder", this.Node.Props.Placeholder);
        }

        input.value = email.Value;

        input.addEventListener("propertychange", () => (email.Value = input.value));
        input.addEventListener("change", () => (email.Value = input.value));
        input.addEventListener("click", () => (email.Value = input.value));
        input.addEventListener("keyup", () => (email.Value = input.value));
        input.addEventListener("input", () => (email.Value = input.value));
        input.addEventListener("paste", () => (email.Value = input.value));
        input.addEventListener("blur", () => (input.value = email.Value));

        field.appendChild(input);

        group
            .appendChild(
                Group(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    slot.Required,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(field);

        return group;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        const slot = this.SlotAssert("email");
        const email = this.DataAssert<string>(instance, slot);

        if (slot.Required && email.Value === "") {
            return false;
        }

        if (email.Value !== "" && !IS_EMAIL.test(email.Value)) {
            return false;
        }

        return true;
    }
}
