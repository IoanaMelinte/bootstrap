import * as Tripetto from "tripetto-collector";
import { ICheckbox, ICheckboxes } from "tripetto-block-checkboxes";
import { Group } from "@components/group";

@Tripetto.node("tripetto-block-checkboxes")
export class Checkboxes extends Tripetto.NodeBlock<HTMLElement, ICheckboxes> {
    public OnRender(instance: Tripetto.Instance, action: Tripetto.Await): HTMLElement {
        const group = document.createElement("div");
        const field = document.createElement("div");
        const table = document.createElement("table");
        const tbody = document.createElement("tbody");

        field.className = "col-sm-12";
        table.className = "table table-striped table-options";

        Tripetto.F.Each(this.Props.Checkboxes, (checkbox: ICheckbox) => {
            const state = this.Data<boolean>(instance, checkbox.Id);
            const tr = document.createElement("tr");
            const td = document.createElement("td");
            const div = document.createElement("div");
            const label = document.createElement("label");
            const input = document.createElement("input");
            const span = document.createElement("span");

            div.className = "checkbox";
            span.textContent = checkbox.Name;
            input.setAttribute("type", "checkbox");

            if (state) {
                input.checked = state.Value;

                input.addEventListener("propertychange", () => (state.Value = input.checked));
                input.addEventListener("change", () => (state.Value = input.checked));
                input.addEventListener("click", () => (state.Value = input.checked));
            }

            label.appendChild(input);
            label.appendChild(span);
            div.appendChild(label);
            td.appendChild(div);
            tr.appendChild(td);
            tbody.appendChild(tr);
        });

        table.appendChild(tbody);
        field.appendChild(table);

        group
            .appendChild(
                Group(
                    this.Node.Props.Id,
                    this.Node.Props.NameVisible ? this.Node.Props.Name : "",
                    false,
                    this.Node.Props.Description,
                    this.Node.Props.Explanation
                )
            )
            .appendChild(field);

        return group;
    }

    public OnValidate(instance: Tripetto.Instance): boolean {
        return true;
    }
}
