import * as Tripetto from "tripetto-collector";
import { Header } from "@components/header";
import { Navigation } from "@components/navigation";
import { Content } from "@components/content";
import { Static } from "@components/static";

export class DOMCollector extends Tripetto.Collector<HTMLElement> {
    private actions: {
        [instance: string]: Tripetto.Await | undefined;
    } = {};

    private DoPrevious(instance: Tripetto.Instance): void {
        const action = this.actions[instance.Hash];

        if (action) {
            action.Cancel();
        }
    }

    private DoNext(instance: Tripetto.Instance): void {
        const action = this.actions[instance.Hash];

        if (action) {
            action.Done();
        }
    }

    public OnInstanceStart(instance: Tripetto.Instance): void {
        const form = document.createElement("form");

        form.id = `form-${instance.Hash}`;
        form.className = "form-horizontal";

        form.appendChild(Header(this.Ontology ? this.Ontology.Name : ""));
        form.appendChild(Content(instance.Hash));
        form.appendChild(Navigation(instance.Hash, () => this.DoNext(instance), () => this.DoPrevious(instance)));

        (document.getElementById("collector") || document.body).appendChild(form);
    }

    public OnInstanceEnd(instance: Tripetto.Instance, type: "ended" | "stopped" | "paused"): void {
        const form = document.getElementById(`form-${instance.Hash}`);

        if (form) {
            form.remove();
        }

        if (type === "ended") {
            // Output the collected data to the console
            console.dir(instance.Values);

            alert("Your form is completed! Now watch the collected data in your browser console.");
        }
    }

    public OnInstanceRender(
        instance: Tripetto.Instance,
        what: Tripetto.Cluster<HTMLElement> | Tripetto.Node<HTMLElement>,
        action: Tripetto.Await
    ): void {
        const content = document.getElementById(`content-${instance.Hash}`);

        if (!content) {
            return;
        }

        if (what instanceof Tripetto.Cluster) {
            this.actions[instance.Hash] = action;

            if (content) {
                const container = document.createElement("div");

                container.className = "container";

                content.appendChild(container);
            }
        } else {
            const container = content.firstElementChild as HTMLElement;

            if (what.Block) {
                // Render the block
                container.appendChild(what.Block.OnRender(instance, action));
            } else {
                // Render static item
                container.appendChild(Static(what.Props.NameVisible ? what.Props.Name : "", what.Props.Description));
            }
        }
    }

    public OnInstanceUnrender(instance: Tripetto.Instance, direction: "forward" | "backward"): void {
        const content = document.getElementById(`content-${instance.Hash}`);

        this.actions[instance.Hash] = undefined;

        if (content) {
            const container = content.firstElementChild;

            if (container) {
                container.remove();
            }
        }
    }

    public OnInstanceStep(instance: Tripetto.Instance, direction: "forward" | "backward"): void {
        const buttonNext = document.getElementById(`button-next-${instance.Hash}`) as HTMLButtonElement | undefined;
        const buttonPrevious = document.getElementById(`button-previous-${instance.Hash}`) as HTMLButtonElement | undefined;

        if (buttonNext) {
            buttonNext.textContent = `${instance.IsAtEnd ? "Complete" : "Next"} `;
        }

        if (buttonPrevious) {
            buttonPrevious.disabled = instance.Steps === 0;
        }
    }

    public OnInstanceValidated(instance: Tripetto.Instance, result: "fail" | "pass"): void {
        const buttonNext = document.getElementById(`button-next-${instance.Hash}`) as HTMLButtonElement | undefined;

        if (buttonNext) {
            buttonNext.disabled = result === "fail";
        }
    }

    public OnInstanceProgress(instance: Tripetto.Instance, percentage: number): void {
        const progressBar = document.getElementById(`progress-bar-${instance.Hash}`);
        const progressLabel = document.getElementById(`progress-label-${instance.Hash}`);

        percentage = Math.floor(percentage);

        if (progressBar) {
            progressBar.setAttribute("aria-valuenow", `${percentage}`);
            progressBar.style.width = `${percentage}%`;
        }

        if (progressLabel) {
            progressLabel.textContent = `${percentage}%`;
        }
    }
}
